package com.zetcode.bean;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CityTest {

	City c = new City("New York", 6000000);

	@Test
	void testCity() {
		c = new City("New York", 6000000);
	}

	@Test
	void testGetId() {
		assertEquals(-1L, c.getId());
	}

	@Test
	void testGetName() {
		assertEquals("New York", c.getName());
	}

	@Test
	void testGetPopulation() {
		assertEquals(6000000, c.getPopulation());
	}

	@Test
	void testToString() {
		assertEquals("City{id=-1, name=New York, population=6000000}", c.toString());
	}

	@Test
	void testHashCode() {
		assertEquals(805075670, c.hashCode());
	}

	@Test
	void testEquals() {
		assertEquals(c, c);
		assertNotEquals(c, new City("New York", 0));
	}

	@Test
	void testCtor() {
		new City();
	}

}
