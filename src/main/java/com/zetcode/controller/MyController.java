package com.zetcode.controller;

import com.zetcode.bean.City;
import com.zetcode.service.ICityService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

	@Autowired
	ICityService cityService;

	@RequestMapping("/cities")
	public List<City> findCities() {
		return (List<City>) cityService.findAll();
	}

	@RequestMapping("/cities/{userId}")
	public City findCity(@PathVariable Long userId) {
		return cityService.findById(userId);
	}
}