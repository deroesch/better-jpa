# better-jpa

All code taken is from <http://zetcode.com/articles/springbootrestdatajpa/>

This is an example of using JPA that actually works.  Nice.

We use the H2 database in memory to catalog a list of cities and their populations.

Run this as a Spring application then hit these URLs:

- <http://localhost:8086/rest/cities>
- <http://localhost:8086/rest/cities/1>

I used PMD for critiquing code cleanup thusly:

`~/tools/pmd-bin-6.22.0-SNAPSHOT/bin/run.sh pmd -d src/ -R rulesets/java/quickstart.xml -f html > example.html`

See: <https://pmd.github.io/pmd/pmd_rules_java.html>